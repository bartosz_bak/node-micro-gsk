export interface Coupon {
    couponID: number;
    couponDiscount: number;
  }